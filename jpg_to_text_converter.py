from PIL import Image
from pytesseract import pytesseract
import os
import pyautogui
import time
import sys
import uuid

def valid_key():
	return_value=False
	uid=uuid.UUID(int=uuid.getnode())
	sys_key=get_key(str(uid))
	try:
		read_key=open("key.txt").readline().rstrip()
		if sys_key==read_key:
			return_value=True
	except:
		print("Need key for System ID:"+str(uid))
	return return_value

def get_key(id):
	key=892138
	data=id.split("-")
	for val in data:
	    for char in val:
	        try:
	            calc_val=int(char)
	            key+=calc_val
	        except:
	            if key>0:
	                key*=9
	            else:
	                key+=9
	return str(key)


def write_file(name,content):
    with open(name, "w") as file1:
        file1.read(content)

if not valid_key():
    print("Please validate your key and try again....!!!")
    input("Press enter to exit...!!")
    sys.exit()
print(os.path.expanduser('~'))
dest_path = os.path.expanduser('~')+os.path.sep+"TEXT_FILES"+os.path.sep
src_path = os.path.expanduser('~')+os.path.sep+"JPEG_FILES"+os.path.sep
if not os.path.isdir(dest_path):
    os.mkdir(dest_path)
if not os.path.isdir(src_path):
    os.mkdir(src_path)
print("$"*80)
print("$"*80)
print(">> Make sure your JPEG file are placed in path:"+src_path)
print(">> Text files will be generated within the path:"+dest_path)
print(">> OLD Files with same name will be be overwtitten")
print("$"*80)
print("$"*80)
print("\n\n")
input("Press enter to continue....!!!")
process_count=0
os.chdir(src_path)dirs = os.listdir()
for dir in dirs:
    process_count+=1
    image_path=src_path+dir
    img = Image.open(image_path)
    print("Reading text from image file:"+image_path)
    text = pytesseract.image_to_string(img)
    name=dest_path+dir.replace(".JPEG",".txt")
    print("Writing text content to file:"+name)
    write_file(name,text[:-1])
    print("Writing completed...!!!\n")

if process_count==0:
    print("\n\nNo data available for processing, please copy the JPEG files and try again...!!!\n")

input("Press enter to exit...!!")
